sap.ui.define([
	"sap/ui/core/UIComponent"
], function (UIComponent) {
	"use strict";

	return UIComponent.extend("siarhei.harkusha.app.Component", {
		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * In this function, call the base component's init function and the router is initialized.
		 */
		init : function () {
			UIComponent.prototype.init.apply(this, arguments);
			
			this.getRouter().initialize();
		}
	});
});