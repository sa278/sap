sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/Fragment",
	"sap/ui/core/Core"
], function(Controller, Fragment, Core){
    "use strict";
    return Controller.extend("siarhei.harkusha.app.controller.BaseController", {
		/**
		 * Convenience method for getting resource bundle values.
		 * 
		 * @param {string} sKey corresponding to the value in the resource bundle
		 * @returns {string} sValue human-readable text
		 */
		i18n: function(sKey){
			var sBundleText = this.getView().getModel("i18n").getResourceBundle().getText(sKey);

			return sBundleText;
		},
		/**
		 * Navigate to a route passed to this function.
		 * 
		 * @param {string} sRoute the name of the route
		 * @param {object} mData the parameters of the route, if the route does not need parameters, it may be omitted.
		 */   
        navigateTo: function(sRoute, mData){
            this.getOwnerComponent().getRouter().navTo(sRoute, mData)
        },
		/**
		 * Activate automatic message generation for complete view
		 */
		initMessageManager: function() {
			var oView = this.getView();
			var oMM   = Core.getMessageManager();

			oMM.registerObject(oView, true);
		},
		/**
		 * Fragment load and getting a dialog  
		 * 
		 * @param {string} sFragmentName name of the fragment
		 * @returns (object) pPromise resolving with the resulting control after fragment parsing and instantiation
		 */
        _getDialog: function(sFragmentName) {
			var pPromise = new Promise(function (resolve, reject) {
				try {
					var oView = this.getView();

					if (!this.oDialog) {
						Fragment
							.load({
								id: oView.getId(),
								name: sFragmentName,
								controller: this
							})	
							.then(function (oDialog) {
								resolve(oDialog);
							});
					} else {
						resolve(this.oDialog);
					}
				} catch (err) {
					reject (new Error(err))
				}
			}.bind(this));

			return pPromise;
		},
		/**
		 * Opens the dialog and adds dependencies for the correct display of the model
		 * 
		 * @param {string} sFragmentName name of the fragment
		 * @param {sap.ui.model.Model} oODataModel the model instance
		 * @param {object} oEntryCtx an OData V2 context object that points to the newly created entry
		 */
        openDialog: function(sFragmentName, oODataModel, oEntryCtx) {
			var oView = this.getView();
			
			this._getDialog(sFragmentName)
				.then(function(oDialog) {
					this.oDialog = oDialog
					oView.addDependent(oDialog);
					this.oDialog.setBindingContext(oEntryCtx);
					this.oDialog.setModel(oODataModel);
					this.oDialog.open();
			}.bind(this))
		},
		/**
		 * Resets pending changes and close the dialog
		 */
		closeDialog: function() {
			var oODataModel = this.getView().getModel("odata");

			oODataModel.resetChanges();
			this.oDialog.close();
		}
    })
});