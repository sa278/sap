sap.ui.define([
	"siarhei/harkusha/app/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Sorter",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"../model/Formatter"
], function (BaseController, Filter, FilterOperator, Sorter, MessageToast, MessageBox, Formatter) {
	"use strict";

	return BaseController.extend("siarhei.harkusha.app.controller.ProductDetails", {
		formatter: Formatter,
		/**
		 * Lifecycle method, which called upon initialization of the View. The controller can perform its internal setup in this hook.
		 * Attaches event handler onPatternMatched to the patternMatched event of this route.
		 * 
		 */
		onInit: function () {
			this.getOwnerComponent().getRouter().getRoute("ProductDetails").attachPatternMatched(this.onPatternMatched, this);
		},
		/**
		 * Using mandatory parameters of the pattern for binding objects, before displaying them 
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onPatternMatched: function (oEvent){
			var sStoreId         = oEvent.getParameter("arguments").storeId;
			var sProductId       = oEvent.getParameter("arguments").productId;
			var that             = this;
			var oEstablishedDate = this.byId("idEstablishedDate");
			var oDataModel       = this.getView().getModel("odata");

			oDataModel.metadataLoaded()
				.then(function(){
					var sStoreKey   = oDataModel.createKey("/Stores", {id: sStoreId});
					var sProductKey = oDataModel.createKey("/Products", {id: sProductId});

					that.getView().bindObject({
						path: sProductKey,
						model: "odata"
					});
					
					oEstablishedDate.bindObject({
						path: sStoreKey,
						model: "odata"
				});
				that.onFilterProduct(sProductId);
			});
		},
		/**
		 * Filtering and sorting the values of the list of comments
		 * 
		 * @param {string} sProductId value of the named parameter
		 */
		onFilterProduct: function (sProductId){
			var oBinding = this.byId("idProductCommentsList").getBinding("items");
			var oFilter  = new Filter("ProductId", FilterOperator.EQ, sProductId); 
			var oSorter  = new Sorter("Posted");

			oBinding.filter(oFilter);
			oBinding.sort(oSorter);
		},
		/**
		 * Navigate to "StoreOverview" View
		 */
		onNavToStoresOverview: function() {
			this.navigateTo("StoresOverview");
		},
		/**
		 * Navigate to "StoreDetails" View
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onNavToStoreDetails: function(oEvent) {
			var oCtx     = oEvent.getSource().getBindingContext("odata");
			var sStoreId = oCtx.getObject().StoreId;

			this.navigateTo("StoreDetails", {storeId: sStoreId});
		},
		/**
		 * Save the data entered by the user
		 */
		onCreateCommentPress:function(){
			var oMsgSuccess = this.i18n("createCommentMessageToastSuccess");
			var oMsgError   = this.i18n("createCommentMessageBoxError");
			var oODataModel = this.getView().getModel("odata");
			var mComment    = {
				"Author": this.byId("idAuthorNameInput").getValue(),
				"Message": this.byId("idFeedinputComment").getValue(),
				"Rating": this.byId("idRatingComment").getValue(),
				"Posted": new Date(),
				"ProductId": this.getView().getBindingContext("odata").getObject("id")
			};

			oODataModel.create("/ProductComments", mComment, {
				success: function(){
					MessageToast.show(oMsgSuccess);
				},
				error: function(){
					MessageBox.error(oMsgError);
				}
			});
		}
	});
});