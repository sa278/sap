sap.ui.define([
	"siarhei/harkusha/app/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Text",
	"sap/ui/model/Sorter",
	"../model/Formatter"
], function (BaseController, Filter, FilterOperator, MessageToast, MessageBox, Dialog, DialogType, Button, ButtonType, Text, Sorter, Formatter) {
	"use strict";
	
	return BaseController.extend("siarhei.harkusha.app.controller.StoreDetails", {
		formatter: Formatter,
		/**
		 * Lifecycle method, which called upon initialization of the View. The controller can perform its internal setup in this hook.
		 * Attaches event handler onPatternMatched to the patternMatched event of this route.
		 */
		onInit: function (){
			this.getOwnerComponent().getRouter().getRoute("StoreDetails").attachPatternMatched(this.onPatternMatched, this);
		},
		/**
		 * Using mandatory parameters of the pattern for binding objects, before displaying them 
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onPatternMatched: function(oEvent){
			var that        = this;
			var oODataModel = this.getView().getModel("odata");
			var sStoreId    = oEvent.getParameter("arguments").storeId;
			
			oODataModel.metadataLoaded().then(function(){
				var sStoreKey = oODataModel.createKey("/Stores", {"id": sStoreId});
				
				that.getView().bindObject({
					path: sStoreKey,
					model: "odata"
				});
			})
		},
		/**
		 * Lifecycle method, which called every time the View is rendered. 
		 * The number of products is calculated by status before they are displayed
		 */
		onBeforeRendering: function() {
			var oJSONModel     = this.getView().getModel("json");
			var oODataModel    = this.getView().getModel("odata");
			var oProductsTable = this.byId("idProductsTable");
			var oListBinding   = oProductsTable.getBinding("items");
			
			oListBinding.attachDataReceived(function() {
				var oCtx        = oProductsTable.getBindingContext("odata");
				var sStoresPath = oODataModel.createKey("/Stores", oCtx.getObject());
				var aStatuses   = ["ALL", "OK", "STORAGE", "OUT_OF_STOCK"];
				
				aStatuses.forEach(function (sStatus) {
					var oParams = {
						success: function (sCount) {
							oJSONModel.setProperty(
								"/" + sStatus.toLowerCase() + "ProductsCount", sCount);
							},
						};
						
					if (sStatus !== "ALL") {
						oParams.filters = [
							new Filter("Status", FilterOperator.EQ, sStatus),
						];
					};
					
					oODataModel.read(sStoresPath + "/rel_Products/$count", oParams);
				});
			});
		},
		/**
		 * Navigate to "StoreOverview" View
		 */
		onNavToStoresOverview: function(){
			this.navigateTo("StoresOverview");
		},
		/**
		 * Select product and navigate to the "ProductDetails" View
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onSelectProductsListItem: function(oEvent) {
			var nStoreId   = oEvent.getSource().getBindingContext("odata").getObject("StoreId");
			var nProductId = oEvent.getSource().getBindingContext("odata").getObject("id");
		
			this.navigateTo("ProductDetails", {
				storeId: nStoreId,
				productId: nProductId
			})	
		},
		/**
		 * Filtering products by selected status
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onFilterStatus: function(oEvent){
			var sKey     = oEvent.getParameter("key");
			var aFilters = [];
			var oBinding = this.byId("idProductsTable").getBinding("items");
		
			if(sKey !== "All") {
				aFilters.push(new Filter("Status", "EQ", sKey));
				oBinding.filter(aFilters);
			} else {
				oBinding.filter(aFilters);
			}
		},			
		/**
		 * Search for products based on user-entered data
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onProductSearch: function(oEvent){
			var sQuery        = oEvent.getParameter("query");
			var sKey          = this.byId("idIconTabBar").getProperty("selectedKey");
			var oBinding      = this.byId("idProductsTable").getBinding("items");
			var oStatusFilter = new Filter("Status", "EQ", sKey);
			var oPriceFilter  = new Filter("Price", FilterOperator.EQ, +sQuery);
			var oFilter       = new Filter({
				filters: [
					new Filter("Name","Contains", sQuery),
					new Filter("Specs", "Contains", sQuery),
					new Filter("SupplierInfo", "Contains", sQuery),
					new Filter("MadeIn", "Contains", sQuery),
					new Filter("ProductionCompanyName", "Contains", sQuery),
				],
				and: false
			});
			var oStringFilter = new Filter({
				filters: [
					oStatusFilter,
					oFilter
				],
				and: true
			});
			var oNumberFilter = new Filter({
				filters: [
					oStatusFilter,
					oPriceFilter
				],
				and: true
			});
		
			if(sKey === "All"){
				isNaN(+sQuery) || +sQuery === 0 ? oBinding.filter(oFilter) : oBinding.filter(oPriceFilter);
			} else {
				isNaN(+sQuery) || +sQuery === 0 ? oBinding.filter(oStringFilter) : oBinding.filter(oNumberFilter);
			};
		},
		/**
		 * Sorting the list of products
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onSortButtonPress: function(oEvent) {
			var sSortField = oEvent.getSource().getProperty('text');
			var oJSONModel = this.getView().getModel("json");
			var oSortItems = oJSONModel.getProperty("/sortItems");
			var sSortType  = oJSONModel.getProperty(`/sortItems/${sSortField}`);
		
			for (let key in oSortItems) {
				oSortItems[key] = "";
			}	
			
			switch (sSortType) {
				case "": {	
					sSortType = "ASC";
					break;
				}
				case "ASC": {
					sSortType = "DESC";
					break;
				}
				case "DESC": {
					sSortType = "";
					break;
				}
			}
		
			oJSONModel.setProperty(`/sortItems/${sSortField}`, sSortType);
		
			var oProductsTable = this.byId("idProductsTable");
			var oItemsBinding = oProductsTable.getBinding("items");
		
			if (sSortType) {
				var bDescending = sSortType === "DESC";
				var oSorter = new Sorter(sSortField, bDescending);
				oItemsBinding.sort(oSorter);
			} else {
				oItemsBinding.sort();			
			}
		},
		/**
		 * Opening a dialog to receive data from the user
		 */
		onOpenCreateNewProductDialod: function(){
			var sFragmentName = "siarhei.harkusha.app.view.fragments.CreateProduct";
			var sMsgTitle     = this.i18n("createProductDialogTitleCreate");
			var oODataModel   = this.getView().getModel("odata");
			var oJSONModel    = this.getView().getModel("json");
			var sStoreId      = this.getView().getBindingContext("odata").getObject("id");
			var oEntryCtx     = oODataModel.createEntry("/Products",{
				properties:{
					StoreId: sStoreId
				}
			});

			oJSONModel.setProperty("/switchDialog/titleDialog", sMsgTitle)
			oJSONModel.setProperty("/switchDialog/edit", false)
			
			this.openDialog(sFragmentName, oODataModel, oEntryCtx);
		},
		/**
		 * Saving data entered by the user
		 */
		onCreateNewProductPress: function(){
			var oDialog           = this.oDialog;
			var oMsgError         = this.i18n("dialogMessageBoxError");
			var oMsgSuccess       = this.i18n("createProductDialogMessageToastSuccess");
			var oODataModel       = this.getView().getModel("odata");
			var oPendingChanges   = oODataModel.getPendingChanges();
			var aEntityValues     = Object.values(oPendingChanges).map(item => Object.values(item)).flat();
			var bValidationResult = aEntityValues.every(item => !!item);
			
			if(bValidationResult){
				oODataModel.submitChanges();
				MessageToast.show(oMsgSuccess)
				oDialog.close()
			}else {
				MessageBox.error(oMsgError);
			}
		},	
		/**
		 * Opening a dialog to confirm the deletion of the store
		 */
		onDeleteStorePress: function(){
			var sMsgTitle        = this.i18n("deleteStoreDialogTitle");
			var sMsgContent      = this.i18n("deleteStoreDialogContent");
			var sMsgButtonDelete = this.i18n("buttonDelete");
			var sMsgButtonCancel = this.i18n("buttonCancel");

			if (!this.oDeleteStoreDialog) {
				this.oDeleteStoreDialog = new Dialog({
					type: DialogType.Message,
					title: sMsgTitle,
					content: new Text({ text: sMsgContent }),
					beginButton: new Button({
						type: ButtonType.Emphasized,
						text: sMsgButtonDelete,
						press: this.deleteStore.bind(this)
					}),
					endButton: new Button({
						text: sMsgButtonCancel,
						press: function () {
							this.oDeleteStoreDialog.close();
						}.bind(this)
					})
				});
			};

			this.oDeleteStoreDialog.open();
		},
		/**
		 * Delete store
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event  
		 */
		deleteStore: function(oEvent){
			var that          = this;
			var oView         = this.getView();
			var sMsgError     = this.i18n("deleteStoreDialogMessageBoxError");
			var sMsgSuccess   = this.i18n("deleteStoreDialogMessageToastSuccess");
			oView.addDependent(this.oDeleteStoreDialog);
			var oODataModel   = oView.getModel("odata");
			var oCtx          = oEvent.getSource().getBindingContext("odata");
			var sStorePath    = oODataModel.createKey("/Stores", oCtx.getObject());

			oODataModel.remove(sStorePath, {
				success: function() {
					MessageToast.show(sMsgSuccess);
					that.navigateTo("StoresOverview");
				},
				error: function () {
					MessageBox.error(sMsgError);
				}
			});
			this.oDeleteStoreDialog.close()
		},
		/**
		 * Opening a dialog for editing a product
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onOpenEditProductDialogPress: function(oEvent){
			var sFragmentName = "siarhei.harkusha.app.view.fragments.CreateProduct";
			var sMsgTitle     = this.i18n("editProductDialogTitle");
			var oODataModel   = this.getView().getModel("odata");
			var oJSONModel    = this.getView().getModel("json");
			var oCtx          = oEvent.getSource().getBindingContext("odata");

			oJSONModel.setProperty("/switchDialog/titleDialog", sMsgTitle)
			oJSONModel.setProperty("/switchDialog/edit", true)

			this.openDialog(sFragmentName, oODataModel, oCtx);
		},
		/**
		 * Saving edited data
		 */
		onSaveEditProductPress: function(){
			var oODataModel = this.getView().getModel("odata");
			
			oODataModel.submitChanges();
			this.oDialog.close();
		},
		/**
		 * Opening a dialog to confirm the deletion of the product
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event 
		 */
		onDeleteProductPress: function(oEvent) {
			var sMsgTitle        = this.i18n("deleteProductDialogTitle");
			var sMsgContent      = this.i18n("deleteProductDialogContent");
			var sMsgDeleteButton = this.i18n("buttonDelete");
			var sMsgCancelButton = this.i18n("buttonCancel");
			var sMsgSuccess      = this.i18n("deleteProductDialogMessageToastSuccess");
			var sMsgError        = this.i18n("deleteProductDialogMessageBoxError");
			var oCtx             = oEvent.getSource().getBindingContext("odata");
			var oODataModel      = oCtx.getModel();
			var sProductPath     = oODataModel.createKey("/Products", oCtx.getObject());
			
			if (!this.oDeleteProductDialog) {
				this.oDeleteProductDialog = new Dialog({
					type: DialogType.Message,
					title: sMsgTitle,
					content: new Text({ text: sMsgContent }),
					beginButton: new Button({
						type: ButtonType.Emphasized,
						text: sMsgDeleteButton,
						press: function(){
							oODataModel.remove(sProductPath, {
								success: function () {
									MessageToast.show(sMsgSuccess)
								},
								error: function () {
									MessageBox.error(sMsgError);
								}
							});
							this.oDeleteProductDialog.close();
						}.bind(this)
					}),
					endButton: new Button({
						text: sMsgCancelButton,
						press: function () {
							this.oDeleteProductDialog.close();
						}.bind(this)
					})
				});
			}
			
			this.oDeleteProductDialog.open();
		}
	});
});