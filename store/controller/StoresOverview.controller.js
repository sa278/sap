sap.ui.define([
	"siarhei/harkusha/app/controller/BaseController",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"../model/CustomEMailType"
], function (BaseController, Filter, FilterOperator, MessageToast, MessageBox, CustomEMailType) {
	"use strict";

	return BaseController.extend("siarhei.harkusha.app.controller.StoresOverview", {
		types : {
			Email: new CustomEMailType()
		},
		/**
		 * Lifecycle method, which called upon initialization of the View. The controller can perform its internal setup in this hook. 
		 * Activate automatic message generation for complete view
		 */
		onInit: function () {
			this.initMessageManager()
		},
		/**
		 * Navigate to "StoreDetails" View
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event
		 */
		onNavToStoreDetails: function (oEvent) {
			var sStoreId = oEvent.getSource().getBindingContext("odata").getObject("id");

			this.navigateTo("StoreDetails", {storeId: sStoreId});
		},
		/**
		 * Searches for stores based on the data entered by the user
		 * 
		 * @param {sap.ui.base.Event} oEvent source of the event
		 */
		onSearch: function (oEvent) {
			var sQuery           = oEvent.getSource().getValue();
			var oStoresList      = this.byId("idStoresList");
			var oBinding         = oStoresList.getBinding("items");
			var oFloorAreaFilter = new Filter("FloorArea", FilterOperator.EQ, +sQuery);
			var oFilter          = new Filter({
				filters: [
					new Filter("Name", FilterOperator.Contains, sQuery),
					new Filter("Address", FilterOperator.Contains, sQuery)
				],
				and: false
			});

			isNaN(+sQuery) || +sQuery === 0 ? oBinding.filter(oFilter) : oBinding.filter(oFloorAreaFilter);
		},
		/**
		 * Opens a dialog for entering data for creating a new store
		 */
		onOpenCreateNewStoreDialod: function(){
			var sFragmentName = "siarhei.harkusha.app.view.fragments.CreateStore";
			var oODataModel   = this.getView().getModel("odata");
			var oEntryCtx     = oODataModel.createEntry("/Stores");

			this.openDialog(sFragmentName, oODataModel, oEntryCtx);
		},
		/**
		 * Checks if all fields are filled in by the user, and then submit сhanges
		 */
		onCreateNewStorePress: function(){
			var oView             = this.getView();
			var oDialog           = this.oDialog;
			var sMsgSuccess       = this.i18n("createStoreDialogMessageToastSuccess"); 
			var sMsgError         = this.i18n("dialogMessageBoxError")
			var aFieldGroup       = oView.getControlsByFieldGroupId("idsCreateNewStoreGroup");
			var aDialogInputs     = aFieldGroup.filter(input => input.sParentAggregationName === "fields");
			var bValidationResult = aDialogInputs.every(input => input.getValue() !== "");
			var oODataModel       = oView.getModel("odata");
			      
			if(bValidationResult){
				oODataModel.submitChanges();
				MessageToast.show(sMsgSuccess)
				oDialog.close()
			}else {
				MessageBox.error(sMsgError);
			}
		},
	});
});