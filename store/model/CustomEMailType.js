sap.ui.define([
	"sap/ui/model/SimpleType",
    "sap/ui/model/ValidateException"
], function (SimpleType, ValidateException) {
	"use strict";

	return SimpleType.extend("siarhei.harkusha.app.model.CustomEMailType", {
		/**
         * does not format the value
         * 
         * @param {string} sValue 
         * @returns {string} sValue
         */
        formatValue: function (sValue) {
            return sValue;
        },
        /**
         * does not parse the value
         * 
         * @param {string} sValue 
         * @returns {string} sValue
         */
        parseValue: function (sValue) {
            return sValue;
        },
        /**
         * Validates the value before sending it to the model
         * 
         * @param {string} sValue entered by the user in the "email" field
         * @returns {string} sValue passed validation 
         */
        validateValue: function (sValue) {
            var rexMail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            
            if (rexMail.test(sValue)) {
                return sValue
            } else{
                throw new ValidateException("'" + sValue + "' is not a valid e-mail");
            } 
        }
	});
});
