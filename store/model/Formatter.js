sap.ui.define([], function () {
	"use strict";

	return {
		/**
		 * Defines the logic of changing the icon on the sort button in the product table.
		 * 
		 * @param {string} sSortType current icon type
		 * @returns {string} sValue with the changed type
		 */
		sortIconFormatter:function(sSortType){
			switch (sSortType) {
				case "": {
					return "sort";
				}
				case "ASC": {
					return "sort-ascending";
				}
				case "DESC": {
					return "sort-descending";
				}
				default: {
					return "sort";
				}
			}
		},
		/**
		 * Changes the color display of the "status" field in the product details according to the incoming status of the product
		 * 
		 * @param {string} sStatus current product status
		 * @returns {string} sValue to be set to the "state" property of the ObjectStatus control
		 */
		stateTypeProductStatusFormatter: function (sStatus) {
			switch (sStatus) {
				case "OK": {
					return "Success";
				}
				case "OUT_OF_STOCK": {
					return "Error";
				}
				case "STORAGE": {
					return "Indication03";
				}
				default: {
					return "Error";
				}
			}
		}
	};
});
